<?php

use \puffin\model as model;
use \feather as feather;

class colors_test extends feather\test
{
	public $x = 0;

	public $colors_obj;

	public function __construct()
	{
		$this->colors_obj = model::load('colors');
	}

	public function test_model_type()
	{
		return $this->assert_class_name( $this->colors_obj, 'colors' );
	}

	public function test_get_colors()
	{
		$arr = $this->colors_obj->get_colors();

		return $this->assert_array_size_equals( $arr, 100 );
	}

	public function test_get_random_colors()
	{
		$c = 41;

		$arr = $this->colors_obj->get_random_colors( $c );

		return $this->assert_array_size_equals( $arr, $c );
	}

	public function test_get_colors_contents()
	{
		$arr = $this->colors_obj->get_colors();

		return ($this->assert_string_starts_with( reset($arr), 'rgb\(' ) && $this->assert_string_ends_with( reset($arr), '\)' ));
	}

}
