<?php

require_once 'colorsbase.php';

class colors extends colorsbase
{
	public function get_colors()
	{
		$arr = range(1,100);
		foreach( $arr as $k => $v )
		{
			$arr[$k] = 'rgb(' . rand(0, 255) .','. rand(0, 255) .','. rand(0, 255) .')';
		}

		return $arr;
	}

	public function get_random_colors( $number_to_get )
	{
		$colors = $this->get_colors();
		shuffle($colors);
		return array_slice($colors, 0, $number_to_get);
	}
}
