<header id="thankyou">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="img/profile.png" alt="">
                <div class="intro-text">
                    <span class="name">THANK YOU</span>
                    <span class="skills">for using The Puffin Framework</span>
                </div>
            </div>
        </div>
    </div>
</header>
