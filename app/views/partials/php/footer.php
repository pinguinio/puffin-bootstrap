<footer class="text-center" id="license">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-12">
                    <h2>Fly Free</h2>
                    <p>Puffin is a free to use, open source framework released under the <a href="https://en.wikipedia.org/wiki/MIT_License" target="_blank">MIT License.</a></p>
                    <br />
                    <div id="mit-license" class="text-justify col-md-8 col-md-push-2"><small>
                        <p>Copyright (c) 2015-2016 Pinguin.com</p>
                        <p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:</p>
                        <p>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.</p>
                        <p>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
                    </small></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h5>Visit Our Website</h5>
                    <a href="http://puffin.pinguin.com" target="_blank" class="btn btn-outline">http://puffin.pinguin.com <i class="fa fa-external-link-square"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
