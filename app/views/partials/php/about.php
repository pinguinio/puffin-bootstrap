<section class="success" id="customizing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Customizing</h2>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2">
                <p>Blah blah blah.</p>
            </div>
            <div class="col-lg-4">
                <p>Blah blah blah.</p>
            </div>
        </div>
    </div>
</section>
