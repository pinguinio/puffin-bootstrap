<!-- Navigation -->
<?= $this->partial('php/navbar', []) ?>

<!-- Header -->
<?= $this->partial('php/header', []) ?>

<!-- About Section -->
<?= $this->partial('php/gettingstarted', []) ?>

<!-- About Section -->
<?= $this->partial('php/about', []) ?>

<!-- Footer -->
<?= $this->partial('php/footer', []) ?>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visible-sm">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>
