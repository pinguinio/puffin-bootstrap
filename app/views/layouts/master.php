<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $this->TITLE ?></title>
		<?php echo $this->META ?>
		<?php echo $this->CSS ?>
		<?php echo $this->JS ?>
	</head>
	<body id="page-top" class="index">
		<?php echo $this->CONTENTS ?>
		<?php echo $this->NONBLOCKING_JS ?>
	</body>
</html>
